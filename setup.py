# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('requirements.txt') as requirements_file:
    requirements = requirements_file.readlines()

setup_requirements = []

test_requirements = []

setup(
    name='data_utils',
    version='0.1.0',
    description="Data utilities",
    long_description=readme + '\n\n',
    author="Sofia Cardita",
    author_email='sofiacardita@gmail.com',
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    keywords='data science',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Framework :: Django',
    ],
    test_suite='test',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
