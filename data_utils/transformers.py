# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import logging
from sklearn.base import BaseEstimator, TransformerMixin

logger = logging.getLogger(__name__)


def run_all_transformers(df, steps):
    # Should normally use a pipeline but useful for testing
    df_ = df.copy()
    for step in steps:
        if isinstance(step, tuple):
            df_ = step[1].transform(df_)  # (name, func)
        else:
            df_ = step.transform(df_)
    return df_


class RemoveColumns(TransformerMixin):
    def __init__(self, col_names=None, copy=False):
        self.col_names = col_names
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        columns = [col for col in df.columns if col in self.col_names]
        return df.drop(columns, axis=1)

    def fit(self, *_):
        return self


class RemoveRows(TransformerMixin):
    def __init__(self, rows=None, drop_by_row_number=True, copy=False):
        self.rows = rows
        self.drop_by_row_number = drop_by_row_number
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        if self.drop_by_row_number:
            return df.drop(df.index[self.rows], axis=0)
        else:
            return df.drop(self.rows, axis=0)

    def fit(self, *_):
        return self


class RenameColumn(TransformerMixin):
    def __init__(self, col, new_name, copy=False):
        self.col = col
        self.new_name = new_name
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        df.rename({self.col: self.new_name}, axis=1)
        return df

    def fit(self, *_):
        return self


class ConvertToDate(TransformerMixin):
    def __init__(self, col_names=None, date_format="%Y-%m-%d", copy=False):
        self.col_names = col_names
        self.copy = copy
        self.date_format = date_format

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        for col_name in self.col_names:
            df[col_name] = pd.to_datetime(X[col_name], format=self.date_format)
        return df

    def fit(self, *_):
        return self


class SplitDateIntoUnits(TransformerMixin):
    def __init__(self, col_names=None, time_units=None, copy=False):
        """
        :param col_names:
        :param time_units: ["year","month","day","dayofweek","dayofyear","weekofyear",
                           "hour","minute","second"]
        """
        self.col_names = col_names
        self.copy = copy
        self.time_units = time_units

    def transform(self, X, *_):
        df = X.copy() if self.copy else X

        for col_name in self.col_names:
            for unit in self.time_units:
                new_name = "%s_%s" % (col_name, unit)
                if unit == "year":
                    df[new_name] = df[col_name].dt.year
                elif unit == "month":
                    df[new_name] = df[col_name].dt.month
                elif unit == "day":
                    df[new_name] = df[col_name].dt.day
                elif unit == "dayofweek":
                    df[new_name] = df[col_name].dt.dayofweek
                elif unit == "dayofyear":
                    df[new_name] = df[col_name].dt.dayofyear
                elif unit == "weekofyear":
                    df[new_name] = df[col_name].dt.weekofyear
                elif unit == "hour":
                    df[new_name] = df[col_name].dt.hour
                elif unit == "minute":
                    df[new_name] = df[col_name].dt.minute
                elif unit == "second":
                    df[new_name] = df[col_name].dt.second
        return df

    def fit(self, *_):
        return self


class ConvertToCyclical(TransformerMixin):
    # http://blog.davidkaleko.com/feature-engineering-cyclical-features.html
    # Dates must have been split into units before hand
    def __init__(self, col_names=None, cyclic_periods=None, copy=False):
        """
        col_names and cyclic periods MUST be aligned

        :param col_names: eg ["date_month", "date_hour"]
        :param cyclic_periods:  [12, 24]
        :param copy:
        """
        self.col_names = col_names
        self.cyclic_periods = cyclic_periods  # in same order as col_names
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        for col_name in self.col_names:
            for period in self.cyclic_periods:
                df[col_name + "_cos"] = np.cos(df[col_name] * (2. * np.pi / period))
                df[col_name + "_sin"] = np.sin(df[col_name] * (2. * np.pi / period))
                # df[col_name + "_cos"] = np.cos(2 * np.pi * df[col_name] / period)
                # df[col_name + "_sin"] = np.sin(2 * np.pi * df[col_name] / period)
        return df

    def fit(self, *_):
        return self


class ConvertToCategorical(TransformerMixin):

    def __init__(self, col_names=None, copy=False):
        self.col_names = col_names
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        for col_name in self.col_names:
            df = pd.concat([df, pd.get_dummies(df[col_name], prefix='dummy_' + col_name)], axis=1)
        return df

    def fit(self, *_):
        return self


class RemoveNAsInColumns(TransformerMixin):
    def __init__(self, copy=False):
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        return df.dropna(axis='columns')

    def fit(self, *_):
        return self


class SetIndexTransformer(TransformerMixin):
    def __init__(self, col_name, copy=False):
        self.copy = copy
        self.col_name = col_name

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        return df.set_index(self.col_name).sort_index()

    def fit(self, *_):
        return self


class FillNas(BaseEstimator, TransformerMixin):
    """
    Impute missing values.

    Binary and object columns are imputed with mode by default.
    Numerical columns are imputed with median by default.

    """
    binary = [0.0, 1.0]
    # https://docs.scipy.org/doc/numpy/user/basics.types.html
    numerical = ["float64", "float32", "float16", "float_",
                 "int64", "int32", "int16", "int8", "int_",
                 "uint64", "uint32", "uint16", "uint8",
                 "complex128", "complex64", "complex_",
                 ]
    target_values = None

    def __init__(self, columns=None, numerical_strategy=None, replacer_value=None, exclude_columns=None,
                 target_col=None):
        self.columns = columns
        self.numerical_strategy = numerical_strategy if numerical_strategy else 'median'
        self.replacer_value = replacer_value
        self.exclude_columns = exclude_columns if exclude_columns else []
        self.target_col = target_col
        self.max_target_values = 20  # control variable to ensure we don't send in target if its a continuous variable

    def do_mode(self, X, col):
        if self.target_col and len(self.target_values) > 1:
            for t in self.target_values:
                mask = X[self.target_col] == t
                v = X[mask][col][np.invert(pd.isnull(mask))].mode().iloc[0]
                logger.info("Filling na's in target %s col %s with mode %s" % (t, col, v))
                X.loc[mask, col] = X.loc[mask, col].fillna(v)
        else:
            v = X[col].mode().iloc[0]
            logger.info("Filling na's in col %s with mode %s" % (col, v))
            X[col] = X[col].fillna(v)

        return X

    def do_median(self, X, col):
        if self.target_col and len(self.target_values) > 1:
            for t in self.target_values:
                mask = X[self.target_col] == t
                v = X[mask][col].median()
                logger.info("Filling na's by target (value %s) in col %s with median %s" % (t, col, v))
                X.loc[mask, col] = X.loc[mask, col].fillna(v)
        else:
            v = X[col].median()
            logger.info("Filling na's in col %s with median %s" % (col, v))
            X[col] = X[col].fillna(v)

        return X

    def do_mean(self, X, col):
        if self.target_col and len(self.target_values) > 1:
            for t in self.target_values:
                mask = X[self.target_col] == t
                v = X[mask][col].mean()
                logger.info("Filling na's in target %s col %s with mean %s" % (t, col, v))
                X.loc[mask, col] = X.loc[mask, col].fillna(v)
        else:
            v = X[col].mean()
            logger.info("Filling na's in col %s with mean %s" % (col, v))
            X[col] = X[col].fillna(v)
        return X

    def transform(self, X):
        df = X.copy()
        self.columns = list(self.columns if self.columns else df.columns)
        for c in self.exclude_columns:
            logger.info("Removing %s from columns" % c)
            try:
                self.columns.remove(c)
            except ValueError:
                pass
        if self.target_col:
            self.target_values = list(df[self.target_col].value_counts().index)
            if 1 < len(self.target_values) > self.max_target_values:
                raise Exception("Max target values reached %s (max %s)" %
                                (len(self.target_values), self.max_target_values))
            target_nans = df[self.target_col].isnull().sum()
            if target_nans > 0:
                raise Exception("Target column has %s nans" % target_nans)

        logger.info("Filling na's in columns %s " % self.columns)
        for col in self.columns:
            if col not in self.exclude_columns:
                value_counts = df[col].value_counts()
                is_binary = list(value_counts.index.values) == self.binary
                dtype = str(df[col].dtype)
                if self.replacer_value:
                    logger.info("Filling na's in col %s with %s" % (col, self.replacer_value))
                    df[col] = df[col].fillna(self.replacer_value)
                elif dtype == "object":
                    logger.warning("Col %s is object type" % col)
                    df = self.do_mode(df, col)
                elif dtype not in self.numerical:
                    logger.warning("Col %s is %s type" % (col, dtype))
                    df = self.do_mode(df, col)
                elif is_binary:
                    df = self.do_mode(df, col)
                elif self.numerical_strategy == "median":
                    df = self.do_median(df, col)
                elif self.numerical_strategy == "mean":
                    df = self.do_mean(df, col)
                elif self.numerical_strategy == "mode":
                    df = self.do_mode(df, col)
                else:
                    raise Exception("Unknown strategy %s for col %s" % (self.numerical_strategy, col))

        return df

    def fit(self, X):
        return self


class RemoveObjectColumns(TransformerMixin):

    def transform(self, X, *_):
        return X.select_dtypes(exclude='object').copy()

    def fit(self, *_):
        return self


class ToDummiesTransformer(BaseEstimator, TransformerMixin):
    """ A Dataframe transformer that provide dummy variable encoding
    """
    def __init__(self, columns=None, drop_original=True, copy=False):
        self.columns = columns
        self.copy = copy
        self.drop_original = drop_original

    def transform(self, X, **transformparams):
        df = X.copy() if self.copy else X
        categorical_columns = [col for col in df.columns if df[col].dtype == 'object']
        columns = self.columns if self.columns else categorical_columns
        logger.info("Transforming to categorical: %s " % columns)
        for col in columns:
            dummies = pd.get_dummies(df[col], prefix=col)
            df = pd.concat([df, dummies], axis=1)
            if self.drop_original:
                df = df.drop(col, axis=1)
        return df

    def fit(self, X, y=None, **fitparams):
        return self


class DropAllZeroTrainColumnsTransformer(BaseEstimator, TransformerMixin):
    """ A DataFrame transformer that provides dropping all-zero columns
    """

    def transform(self, X, **transformparams):
        trans = X.drop(self.columns_, axis=1).copy()
        return trans

    def fit(self, X, y=None, **fitparams):
        """ Determines the all-zero columns of X
        """
        self.columns_ = X.columns[(X == 0).all()]
        return self


class DropAllNansColumnsRowsTransformer(TransformerMixin):
    """ A DataFrame transformer that provides dropping all-nans columns
    """
    def __init__(self, axis=1, copy=False):
        self.copy = copy
        self.axis = axis  # 1 for columns, 0 for rows

    def transform(self, X, **transformparams):
        df = X.copy() if self.copy else X
        df = df.dropna(axis=self.axis, how='all')
        return df

    def fit(self, X, y=None, **fitparams):
        return self


class ChangeTypeTransformer(TransformerMixin):
    def __init__(self, columns, dtype, copy=False):
        self.columns = columns
        self.dtype = dtype
        self.copy = copy

    def transform(self, X, **transformparams):
        df = X.copy() if self.copy else X
        columns = self.columns if self.columns else X.columns
        for col in columns:
            df[col] = df[col].astype(self.dtype)
        return df

    def fit(self, X, y=None, **fitparams):
        return self


class ReplaceTransformer(TransformerMixin):
    def __init__(self, columns, replace, replace_with, regex=False, copy=False):
        self.columns = columns
        self.replace = replace
        self.replace_with = replace_with
        self.copy = copy
        self.regex = regex

    def transform(self, X, **transformparams):
        df = X.copy() if self.copy else X
        for col in self.columns:
            df[col] = df[col].replace(self.replace, self.replace_with, regex=self.regex)
        return df

    def fit(self, X, y=None, **fitparams):
        return self


class AddComputedColumn(TransformerMixin):
    def __init__(self, new_col, func, prefix="NEW_", copy=False):
        self.new_col = prefix + new_col
        self.func = func
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        df[self.new_col] = self.func(df)
        return df

    def fit(self, *_):
        return self


class GroupBy(TransformerMixin):
    def __init__(self, group_by_col, aggregations, prefix="AGG_", copy=False):
        """
        :param group_by_col:
        :type aggregations: Union[str, dict]
        :param aggregations: either a dict or a default function, eg mean that will be applied to ALL columns
        :param prefix:
        :param copy:
        """
        self.group_by_col = group_by_col
        self.prefix = prefix
        self.aggregations = aggregations
        self.copy = copy

    def transform(self, X, *_):
        df = X.copy() if self.copy else X
        aggregations = self.aggregations
        if isinstance(aggregations, str):
            logger.info("Applying %s to all columns" % aggregations)
            aggregations = dict.fromkeys(df.columns, self.aggregations)
        logger.info("Aggregating %s. Columns: %s" % (aggregations, df.columns))
        df_agg = df.groupby(self.group_by_col).agg({**aggregations})
        df_agg.columns = pd.Index([self.prefix + e[0] + "_" + e[1].upper() for e in df_agg.columns.tolist()])
        return df_agg

    def fit(self, *_):
        return self


class Debug(TransformerMixin):
    def __init__(self, do_info=True, do_describe=False):
        self.do_info = do_info
        self.do_describe = do_describe

    def transform(self, X, *_):
        print("---START DEBUG ---")
        if self.do_info:
            print("INFO")
            print(X.info())
        if self.do_describe:
            print("DESCRIBE")
            print(X.describe())
        print("SHAPE")
        print(X.shape)
        print("---END DEBUG ---")
        return X

    def fit(self, *_):
        return self
