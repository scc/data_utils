# -*- coding: utf-8 -*-


import logging
import os
import time
from contextlib import contextmanager

level = os.getenv("LOGLEVEL", "INFO")
logging.basicConfig(level=level)


@contextmanager
def timer(title):
    t0 = time.time()
    yield
    print("{} - done in {:.0f}s".format(title, time.time() - t0))


