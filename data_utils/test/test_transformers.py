# -*- coding: utf-8 -*-

from unittest import TestCase
import pandas as pd
import numpy as np
from data_utils.transformers import RemoveColumns, ConvertToDate, SplitDateIntoUnits, run_all_transformers, \
    ConvertToCyclical, FillNas, RemoveObjectColumns
from sklearn.preprocessing import Imputer
from sklearn.pipeline import make_pipeline


class TestDropColumnTransformer(TestCase):
    def test_drop_single_column(self):

        d = {'col1': [1, 2, 3], 'col2': [10, 20, 30]}
        df = pd.DataFrame(data=d)

        transformer = RemoveColumns(col_names=["col1"])
        df_ = transformer.transform(df)
        self.assertEqual(len(df_.columns), 1)
        self.assertEqual(df_.col2[0], 10)

    def test_drop_columns(self):

        d = {
            'col1': [1, 2, 3],
            'col2': [10, 20, 30],
            'col3': [100, 200, 300],
            'col4': [1000, 2000, 3000],
        }
        df = pd.DataFrame(data=d)

        transformer = RemoveColumns(col_names=["col1", "col3"])
        df_ = transformer.transform(df)
        self.assertEqual(len(df_.columns), 2)
        self.assertEqual(df_.col2[0], 10)


class TestConvertToDate(TestCase):
    def test_convert_col_to_date(self):

        d = {'col1': ["1978-06-25", "1967-01-02", "1999-09-01"], 'col2': [10, 20, 30]}
        df = pd.DataFrame(data=d)

        transformer = ConvertToDate(col_names=["col1"])
        df_ = transformer.transform(df)
        self.assertEqual(len(df_.columns), 2)
        self.assertEqual(str(df_.col1.dtype), "datetime64[ns]")


class TestSplitIntoDate(TestCase):
    def test_split_into_year(self):

        d = {'date': ["1978-06-25", "1967-01-02", "1999-09-01"], 'values': [10, 20, 30]}
        df = pd.DataFrame(data=d)

        steps = [
            ConvertToDate(col_names=["date"]),
            SplitDateIntoUnits(col_names=["date"], time_units=["year", "month"]),
        ]

        df_ = run_all_transformers(df, steps)
        self.assertEqual(len(df_.columns), 4)
        self.assertEqual(df_.date_year[0], 1978)
        self.assertEqual(df_.date_month[2], 9)


class TestConvertToCyclical(TestCase):
    def test_cyclical(self):

        d = {'date': ["1978-06-25 00:00:00", "1967-01-02 23:13:59", "1999-12-01 14:34:01"], 'values': [10, 20, 30]}
        df = pd.DataFrame(data=d)

        steps = [
            ConvertToDate(col_names=["date"], date_format="%Y-%m-%d %H:%M:%S"),
            SplitDateIntoUnits(col_names=["date"], time_units=["hour", "month"]),
            ConvertToCyclical(col_names=["date_hour", "date_month"], cyclic_periods=[24, 12]),
        ]

        df_ = run_all_transformers(df, steps)
        self.assertEqual(len(df_.columns), 8)


class TestFillNas(TestCase):

    def test_fill_nas(self):
        d = {
            'stringy': ["a", "b", "a", np.nan],
            'binary': [1, 2, 1, np.nan],
            'inty': [10, 20, 30, np.nan],
            'inty2': [10, 15, 30, np.nan],
            'floaty': [np.nan, 100.0, 200.0, 300.0],
            'col4': [1000, 2000, 3000, np.nan],
        }
        df = pd.DataFrame(data=d)
        pipeline = make_pipeline(
            FillNas(exclude_columns=["col4", "inty2"]),
            FillNas(columns=["col4"], replacer_value=5000),
            FillNas(columns=["inty2"], numerical_strategy="mean"),
        )

        df_ = pipeline.fit_transform(df)
        self.assertEqual(df_["stringy"][3], "a")
        self.assertEqual(df_["binary"][3], 1)
        self.assertEqual(df_["inty"][3], df["inty"].median())
        self.assertEqual(df_["inty2"][3], df["inty2"].mean())
        self.assertEqual(df_["col4"][3], 5000)
        self.assertEqual(df_.isnull().sum().sum(), 0)

    def test_fill_nas_by_target(self):
        d = {
            'stringy': ["a", "b", "a", np.nan, "b", "b"],
            'inty': [10, 20, 30, np.nan, 4, 5],
            'inty2': [10, 15, 30, np.nan, 100, 200],
            'floaty': [np.nan, 100.0, 200.0, 300.0, 1, 2],
            'col4': [1000, 2000, 3000, np.nan, 4, 5],
            'target': [1, 1, 1, 2, 2, 2],
        }
        df = pd.DataFrame(data=d)
        pipeline = make_pipeline(
            FillNas(target_col="target", exclude_columns=["target"]),
        )

        df_ = pipeline.fit_transform(df)
        self.assertEqual(df_["stringy"][3], "b")
        self.assertEqual(df_["inty"][3], 4.5)
        self.assertEqual(df_["inty2"][3], 150)
        self.assertEqual(df_["col4"][3], 4.5)
        self.assertEqual(df_["floaty"][0], 150)
        self.assertEqual(df_.isnull().sum().sum(), 0)

