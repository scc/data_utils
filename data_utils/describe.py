# Initially from Pedro Adão's code, then modified


def describe_columns(_df):
    names = list(_df.dtypes.index.get_values())
    types = list(_df.dtypes)
    total_rows = _df.shape[0]

    for i, col in enumerate(names):

        value_counts = _df[col].value_counts()
        total_values = len(value_counts)
        if total_values <= 4:  # display values list if nr is small
            vlist = str(_df[col].value_counts().index.values)
        else:
            vlist = "-"

        nans = _df[col].isnull().sum()

        s = "%s. %s : %s (total_values: %s; values: %s) (nans: %s)" % (i, names[i], types[i], total_values, vlist, nans)

        # Warnings
        if types[i] == "object":
            s += " -------- Object!!"
        percent_nans = (nans * 100) / total_rows
        if percent_nans >= 50:
            s += "; High nans ({pr}%)!!".format(pr='{0:.2f}'.format(percent_nans))

        print(s)


def describe_data(_df, name="data"):
    print("Describing '%s'" % name)
    print("type: ", str(type(_df)).translate("".maketrans("", "", "<>")))
    print("shape: ", _df.shape)
    print(".dtypes:")
    describe_columns(_df)
    print("Total nans", _df.isnull().sum().sum())
    print(".index.names: ", list(_df.index.names))
