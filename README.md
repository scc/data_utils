# Personal collection of data utils

### TODO

- Look into https://github.com/iskandr/fancyimpute for better imputing methods.
- Look into https://github.com/WillKoehrsen/feature-selector to highlight colinear features and low/high importance features.


### Describe dataframe


```
describe.describe_data(df, name="housing")
```

We get something like this:

```
Describing 'housing'
type:  class 'pandas.core.frame.DataFrame'
shape:  (100000, 91)
.dtypes:
0. TOTALAREA_MODE : float64 (total_values: 4362; values: -) (nans: 48133)
1. FLOORSMIN_AVG : float64 (total_values: 229; values: -) (nans: 67723); High nans (67.723%)!!
2. FLOORSMIN_MODE : float64 (total_values: 25; values: -) (nans: 67723); High nans (67.723%)!!
3. ORGANIZATION_TYPE : object (total_values: 58; values: -) (nans: 0) -------- Object!!
4. NONLIVINGAREA_AVG : float64 (total_values: 2524; values: -) (nans: 54995); High nans (54.995%)!!
5. HOUSETYPE_MODE : object (total_values: 3; values: ['block of flats' 'specific housing' 'terraced house']) (nans: 49984) -------- Object!!
```

The main advantage is that it highlights columns with object data types and with a high percentage of missing values.
Note that it only prints the values themselves if the total values are <=4 (see housetype_mode above).

### Transformers

Having this data frame:

```
   binary    col4  floaty  inty  inty2 stringy
0     1.0  1000.0     NaN  10.0   10.0       a
1     2.0  2000.0   100.0  20.0   15.0       b
2     1.0  3000.0   200.0  30.0   30.0       a
3     NaN     NaN   300.0   NaN    NaN     NaN
```


Run a pipeline:

```
pipeline = make_pipeline(
    FillNas(exclude_cols=["col4", "inty2"]),
    FillNas(columns=["col4"], replacer_value=5000),
    FillNas(columns=["inty2"], numerical_strategy="mean"),
)

df_ = pipeline.fit_transform(df)
```

We end up with this dataframe:

```
   binary    col4  floaty  inty      inty2 stringy
0     1.0  1000.0   200.0  10.0  10.000000       a
1     2.0  2000.0   100.0  20.0  15.000000       b
2     1.0  3000.0   200.0  30.0  30.000000       a
3     1.0  5000.0   300.0  20.0  18.333333       a
```

Another example of a more complete pipeline:

```
pipeline = make_pipeline(
    transformers.ReplaceTransformer(["FLAG_DOCUMENT_19"], replace="Yay", replace_with=1),
    transformers.ReplaceTransformer(["FLAG_DOCUMENT_19"], replace="Nay", replace_with=0),
    transformers.ReplaceTransformer(["AMT_GOODS_PRICE"], replace="€", replace_with="", regex=True),
    transformers.ChangeTypeTransformer(["AMT_GOODS_PRICE"], dtype="int64"),
    transformers.ReplaceTransformer(df.columns, replace=np.inf, replace_with=np.nan),
    transformers.ReplaceTransformer(df.columns, replace=-np.inf, replace_with=np.nan),
    transformers.ReplaceTransformer(["EXT_SOURCE_2"], replace="%", replace_with="", regex=True),
    transformers.ChangeTypeTransformer(["EXT_SOURCE_2"], dtype="float64"),
    transformers.RemoveColumns(["NAME_TYPE_SUITE", "OCCUPATION_TYPE.1", "REGION_RATING_CLIENT"]),
    transformers.ReplaceTransformer(["LIVINGAREA_AVG"], replace=-99999.0000, replace_with=np.nan),
    transformers.ReplaceTransformer(["LIVINGAREA_AVG"], replace=1, replace_with=np.nan),
    transformers.FillNas(["LIVINGAREA_AVG"], numerical_strategy="mean"),
    transformers.FillNas(exclude_columns=["TARGET"]),
    transformers.ToDummiesTransformer(["ORGANIZATION_TYPE", "HOUSETYPE_MODE", "WEEKDAY_APPR_PROCESS_START",
                                         "NAME_HOUSING_TYPE", "NAME_CONTRACT_TYPE", "WALLSMATERIAL_MODE",
                                      "FLAG_OWN_REALTY","OCCUPATION_TYPE", "CODE_GENDER", "NAME_FAMILY_STATUS",
                                      "FLAG_OWN_CAR"]),
)
df_ = pipeline.fit_transform(df)

# Another example

bureau_pipeline = make_pipeline(
        transformers.RemoveColumns(["SK_ID_BUREAU"]),
        transformers.ToDummiesTransformer(),
        transformers.GroupBy(index_col,
                             {
                                'DAYS_CREDIT': ['min', 'max', 'mean', 'var'],
                                'DAYS_CREDIT_ENDDATE': ['min', 'max', 'mean'],
                                'DAYS_CREDIT_UPDATE': ['mean'],
                                'CREDIT_DAY_OVERDUE': ['max', 'mean'],
                                'AMT_CREDIT_MAX_OVERDUE': ['mean'],
                                'AMT_CREDIT_SUM': ['max', 'mean', 'sum'],
                                'AMT_CREDIT_SUM_DEBT': ['max', 'mean', 'sum'],
                                'AMT_CREDIT_SUM_OVERDUE': ['mean'],
                                'AMT_CREDIT_SUM_LIMIT': ['mean', 'sum'],
                                'AMT_ANNUITY': ['max', 'mean'],
                                'CNT_CREDIT_PROLONG': ['sum'],
                                'MONTHS_BALANCE_MIN': ['min'],
                                'MONTHS_BALANCE_MAX': ['max'],
                                'MONTHS_BALANCE_SIZE': ['mean', 'sum']
                            },
                            prefix="BUREAU"),
        #transformers.FillNas(exclude_columns=["TARGET"]),
        transformers.DropAllNansColumnsRowsTransformer(),
        transformers.DropAllNansColumnsRowsTransformer(axis=0),
        # nans > 60%
        transformers.RemoveColumns(["BUREAUAMT_CREDIT_MAX_OVERDUE_MEAN",
                                    "BUREAUAMT_ANNUITY_MAX", "BUREAUAMT_ANNUITY_MEAN"]),
        transformers.FillNas(numerical_strategy="mean")
    )
bureau_bal = bureau_pipeline.fit_transform(bureau_bal)
```


## Run tests

```
python -m unittest discover
```
